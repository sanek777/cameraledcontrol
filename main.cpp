#include <iostream>
#include "server.h"

using namespace std;

// Имена файлов водного и выходного каналов
static const char *inputFIFO = "/var/tmp/camledInput";
static const char *outputFIFO = "/var/tmp/camledOutput";


int main(int argc, char *argv[])
{
    cout << "Camera led server started!" << endl;

    CameraLedServer server(inputFIFO, outputFIFO);

    return server.run();
}
