#ifndef SERVER_H
#define SERVER_H

#include <string>
#include <vector>
#include "commands.h"

using namespace std;

typedef vector<Command *> Commands;
typedef vector<string> Strings;

/**
 *      Клаас сервера управления камерой
 */
class CameraLedServer
{
private:
    const char *inputFifoName;
    const char *outputFifoName;
    Commands commands;

private:
    void fatalError(const string &error);
    string executeCommand(const string &cmd);
    Strings readInputFifo();
    void writeOutputFifo(const string &str);

public:
    CameraLedServer(const char *inFifo, const char *outFifo);
    virtual ~CameraLedServer();

    int run();
};


#endif // SERVER_H
