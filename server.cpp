#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sys/stat.h> 
#include <unistd.h>
#include <fcntl.h> 
#include <csignal>
#include "server.h"


// Максимальная длина входного потока
#define MAX_INPUT_LEN 512


// Обработка нажатия Ctrl+C
volatile sig_atomic_t interrupt = 0;    // флаг прерывания
static void signal_interrupt(int sig)
{
    interrupt = 1; // установка флага прерывания
}


/**
 *      CameraLedServer
 *      Конструктор
 */
CameraLedServer::CameraLedServer(const char *inFifo, const char *outFifo):
    inputFifoName(inFifo),
    outputFifoName(outFifo)
{
    // Добавление команд в список
    commands.push_back(new SetLedStateCommand());
    commands.push_back(new GetLedStateCommand());
    commands.push_back(new SetLedColorCommand());
    commands.push_back(new GetLedColorCommand());
    commands.push_back(new SetLedRateCommand());
    commands.push_back(new GetLedRateCommand());

    // Создание входного канала
    unlink(inputFifoName);
    if (mkfifo(inputFifoName, S_IRUSR|S_IWUSR) == -1) {
        fatalError("Error make input FIFO '"+string(inputFifoName)+"!\n");
    }

    // Создание выходного канала
    unlink(outputFifoName);
    if (mkfifo(outputFifoName, S_IRUSR|S_IWUSR) == -1) {
        fatalError("Error make output FIFO '"+string(outputFifoName)+"!\n");
    }

    // Перехват Ctrl+C
    struct sigaction sig_int_handler;
    sig_int_handler.sa_handler = signal_interrupt;
    sigemptyset(&sig_int_handler.sa_mask);
    sig_int_handler.sa_flags = 0;

    sigaction(SIGINT, &sig_int_handler, NULL);


    cout << "For EXIT press Ctrl+C" << endl;
}


/**
 *      CameraLedServer
 *      Деструктор
 */
CameraLedServer::~CameraLedServer()
{
    cout << "Server shutdown..." << endl;

    // удаление файлов потоков
    unlink(inputFifoName);
    unlink(outputFifoName);

    // очистка списка команд
    Commands::const_iterator it = commands.begin();
    while(it != commands.end()) {
        delete *it;
        ++it;
    }

    commands.clear();
}


/**
 *      CameraLedServer
 *      Функция завершения работы по критической ошибке
 */
void CameraLedServer::fatalError(const string &error)
{
    cerr << "FATAL ERROR: " << error;
    exit(1);
}


/**
 *      CameraLedServer
 *      Выполнение команды
 */
string CameraLedServer::executeCommand(const string &cmd)
{
    cout << "Receive command: " << cmd << endl;

    Commands::const_iterator it = commands.begin();
    while(it != commands.end()) {
        Command *command = *it;
        if (command->execute(cmd)) {
            cout << "RESULT: " << command->result() << endl;
            return command->result();
        }
        ++it;
    }
    cout << "Command '" << cmd << "' not found!" << endl;
    return string();
}


/**
 *      CameraLedServer
 *      Чтение строк из входящего канала
 */
Strings CameraLedServer::readInputFifo()
{
    int fd = open(inputFifoName, O_RDONLY);
    if (!fd)
        fatalError("Error open input FIFO '"+string(inputFifoName)+"'!\n");

    char buf[MAX_INPUT_LEN];
    int size = read(fd, buf, MAX_INPUT_LEN);
    close(fd);

    Strings inputData;
    if (!interrupt) {
        if (size == -1)
            fatalError("Error read data from FIFO!\n");

        string s;
        for (int i = 0; i < size; ++i) {
            if (buf[i] == '\n') {
                inputData.push_back(s);
                s.clear();
            }
    
            s += (buf[i]);
        }
    }

    return inputData;
}
    
    
/**
 *      CameraLedServer
 *      Запись строки в выходной канал
 */
void CameraLedServer::writeOutputFifo(const string &str)
{
    if (str.empty())
        return;

    int fd = open(outputFifoName, O_WRONLY);
    if (!fd)
        fatalError("Error open output FIFO '"+string(outputFifoName)+"'!\n");

    int len = str.length();
    char buf[255];
    memcpy(buf, str.c_str(), len);
    buf[len] = '\n';
    
    int size = write(fd, buf, len+1);
    close(fd);

    if (size != len+1)
        fatalError("Error write data to FIFO!\n");
}


/**
 *      CameraLedServer
 *      Основной цикл выполнения
 */
int CameraLedServer::run()
{
    while (!interrupt) {
        Strings input = readInputFifo();
        Strings::const_iterator it = input.begin();
        while (it != input.end()) {
            string result = executeCommand(*it);
            writeOutputFifo(result);
            ++it;
        }
    }
    return 0;
}