#ifndef LEDCONTROLLER_H
#define LEDCONTROLLER_H

#include <string>

using namespace std;


/**
 *      Класс управления контроллером светодиода камеры
 */
class LedController
{
private:
    static int ledState;
    static int ledRate;
    static string ledColor;

    static LedController ledController;

private:
    LedController();
    LedController(LedController&);
    LedController(const LedController&);

public:
    // Состояние светодиода
    enum LedState {
        LED_STATE_OFF = 0,      // выключен
        LED_STATE_ON,           // включен
        LED_STATE_UNDEFINED     // неопределенное состояние
    };

    static const string LED_COLOR_UNDEFINED;    // цвет не определен
    static const int LED_RATE_UNDEFINED;        // частота мерцания не определена

public:
    virtual ~LedController();

    // Функции управления цветом светодиода
    static bool setColor(const string &color);
    static string color();
    
    // Функции управления состоянием светодиода
    static bool setState(const int state);
    static int state();

    // Функции управления мерцанием светодиода
    static bool setRate(const int rate);
    static int rate();
};


#endif // LEDCONTROLLER_H
