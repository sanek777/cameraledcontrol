#include <cstdlib>
#include "commands.h"
#include "ledcontroller.h"


const string Command::OK("OK");
const string Command::FAILED("FAILED");


/**
 *      SetLedStateCommand
 *      Исполнение команды установки состояния светодиода
 */
bool SetLedStateCommand::execute(const string &cmd) {
    if (!validate(cmd))
        return false;

    commandResult = FAILED;

    string argState = cmd.substr(command.length()+1);

    if (argState == "on" && LedController::setState(LedController::LED_STATE_ON)) 
        commandResult = OK;
    else if (argState == "off" && LedController::setState(LedController::LED_STATE_OFF))
        commandResult = OK;

    return true;
}


/**
 *      GetLedStateCommand
 *      Исполнение команды получения состояния светодиода
 */
bool GetLedStateCommand::execute(const string &cmd) {
    if (!validate(cmd))
        return false;

    int state = LedController::state();

    if (state == LedController::LED_STATE_ON)
        commandResult = OK + " on";
    else if (state == LedController::LED_STATE_OFF)
        commandResult = OK + " off";
    else
        commandResult = FAILED;

    return true;
}


/**
 *      SetLedColorCommand
 *      Исполнение команды установки цвета светодиода
 */
bool SetLedColorCommand::execute(const string &cmd) {
    if (!validate(cmd))
        return false;

    string argColor = cmd.substr(command.length()+1);

    if (LedController::setColor(argColor))
        commandResult = OK;
     else
        commandResult = FAILED;

    return true;
}


/**
 *      GetLedColorCommand
 *      Исполнение команды получения цвета светодиода
 */
bool GetLedColorCommand::execute(const string &cmd) {
    if (!validate(cmd))
        return false;

    string color = LedController::color();

    if (color == LedController::LED_COLOR_UNDEFINED)
        commandResult = FAILED;
     else
        commandResult = OK + " " + color;

    return true;
}


/**
 *      SetLedRateCommand
 *      Исполнение команды установки частоты мерцания светодиода
 */
bool SetLedRateCommand::execute(const string &cmd) {
    if (!validate(cmd))
        return false;

    const string arg = cmd.substr(command.length()+1);

    const char *s = arg.c_str();
    char *s_end;
    int rate = strtol(s, &s_end, 10);

    if (s != s_end && LedController::setRate(rate))
        commandResult = OK;
    else
        commandResult = FAILED;

    return true;
}


/**
 *      GetLedRateCommand
 */
bool GetLedRateCommand::execute(const string &cmd) {
    if (!validate(cmd))
        return false;

    int rate = LedController::rate();

    if (rate == LedController::LED_RATE_UNDEFINED)
        commandResult = FAILED;
     else
        commandResult = OK + " " + to_string(rate);

    return true;
}
