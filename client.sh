######################################################
#
#   Скрипт управления светодиодом камеры
#
######################################################
#!/bin/bash

# имена каналов ввода-вывода
INPUT_FIFO="/var/tmp/camledInput"
OUTPUT_FIFO="/var/tmp/camledOutput"

# Функция отображения главного меню
function mainMenu {
    clear
    echo "*******************************************************"
    echo
    echo -e "\tУправление светодиодом камеры:\n"
    echo -e "\t1. Изменить состояние светодиода"
    echo -e "\t2. Запросить состояние светодиода"
    echo -e "\t3. Изменить цвет светодиода"
    echo -e "\t4. Запросить цвет светодиода"
    echo -e "\t5. Изменить частоту мерцания светодиода"
    echo -e "\t6. Запросить частоту мерцания светодиода"
    echo -e "\t0. Выход"
    echo
    echo "*******************************************************"
    echo
    echo -n "Введите номер функции: "
    read -n 1 option
}


# функция отображения меню изменения состояния светодиода
function changeStateMenu {
    clear
    echo "*******************************************************"
    echo
    echo -e "\tСмена состояния светодиода\n"
    echo -e "\t1. Включить светодиод"
    echo -e "\t2. Выключить светодиод"
    echo -e "\t3. Отмена"
    echo
    echo "*******************************************************"
    echo
    echo -n "Введите номер функции: "
    read -n 1 option
}


# функция отображения меню изменения цвета светодиода
function changeColorMenu {
    clear
    echo "*******************************************************"
    echo
    echo -e "\tВыбирите цвет светодиода:\n"
    echo -e "\t1. Красный"
    echo -e "\t2. Синий"
    echo -e "\t3. Зеленый"
    echo -e "\t4. Отмена"
    echo
    echo "*******************************************************"
    echo
    echo -n "Введите номер цвета: "
    read -n 1 option
}


# функция изменения состояния светодиода
function changeSate {
    while [ $? -ne 1 ]
    do
        changeStateMenu
        case $option in
            1)
            STATE="on"
            break ;;
            2)
            STATE="off"
            break ;;
            3)
            return ;;
            *)
            continue ;;
        esac
    done

    clear
    echo "set-led-state $STATE">> $INPUT_FIFO
    read -r line < $OUTPUT_FIFO
    if [ ! -z $line ] && [ $line = "OK" ]
    then
        echo "*******************************************************" 
        echo -e "\n\tСостояние светодиода успешно изменёно!\n"
        echo "*******************************************************" 
    else
        echo "*******************************************************" 
        echo -e "\n\tОшибка при изменении состояния светодиода!\n"
        echo "*******************************************************" 
    fi
}


# функция запроса состояния светодиода
function requestState {
    clear
    echo "get-led-state">> $INPUT_FIFO
    read -r -a line < $OUTPUT_FIFO
    if [ ! -z ${line[0]} ] && [ ${line[0]} = "OK" ] && [ ! -z ${line[1]} ]
    then
        if [ ${line[1]} = "on" ]
        then
            STATE="включен"
        else
            STATE="выключен"
        fi
        echo -e "*******************************************************" 
        echo -e "\n\tСостояние светодиода: $STATE\n"
        echo -e "*******************************************************" 
    else
        echo -e "*******************************************************" 
        echo -e "\n\tОшибка запроса состояния светодиода!\n"
        echo -e "*******************************************************" 
    fi
}


# функция изменения цвета светодиода
function changeColor {
    while [ $? -ne 1 ]
    do
        changeColorMenu
        case $option in
        1)
            COLOR="red"
            break ;;
        2)
            COLOR="blue"
            break ;;
        3)
            COLOR="green"
            break ;;
        4)
            return ;;
        *)
            continue ;;
        esac
    done
    clear
    echo "set-led-color $COLOR">> $INPUT_FIFO
    read -r line < $OUTPUT_FIFO
    if [ ! -z $line ] && [ $line = "OK" ]
    then
        echo "*******************************************************" 
        echo -e "\n\tЦвет светодиода успешно изменён!\n"
        echo "*******************************************************" 
    else
        echo "*******************************************************" 
        echo -e "\n\tОшибка при изменении цвета светодиода\n"
        echo "*******************************************************" 
    fi
}


# функция апроса цвета светодиода
function requestColor {
    clear
    echo "get-led-color">> $INPUT_FIFO
    read -r -a line < $OUTPUT_FIFO
    if [ ! -z ${line[0]} ] && [ ${line[0]} = "OK" ] && [ ! -z ${line[1]} ]
    then
        case ${line[1]} in
        red)
            COLOR="красный" ;;
        green)
            COLOR="зеленый" ;;
        blue)
            COLOR="синий" ;;
        esac
        echo "*******************************************************" 
        echo -e "\n\tЦвет светодиода: $COLOR\n"
        echo "*******************************************************" 
    else
        echo "*******************************************************" 
        echo -e "\n\tОшибка запроса цвета светодиода!\n"
        echo "*******************************************************" 
    fi
}

# функция изменения частоты мерцания светодиода
function changeRate {
    while true
    do
        clear
        echo "*******************************************************" 
        echo -e "\n\tВведите частоту мерцания светодиода (0-5 Гц)\n"
        echo -e "\tВведите 9 для отмены"
        echo -e "*******************************************************\n"
        echo -n "[Гц]: " 
        read -n 1 rate

        if [ $rate -eq 9 ]
        then
            return
        fi

        if [ $rate -ge 0 ] && [ $rate -le 5 ]
        then
            clear
            echo "set-led-rate $rate">> $INPUT_FIFO
            read -r line < $OUTPUT_FIFO
            if [ ! -z $line ] && [ $line = "OK" ]
            then
                echo "*******************************************************" 
                echo -e "\n\tЧастота мерцания светодиода успешно изменена!\n"
                echo "*******************************************************" 
            else
                echo "*******************************************************" 
                echo -e "\nОшибка при изменении частоты мерцания светодиода\n"
                echo "*******************************************************" 
            fi
            break
        fi
    done
}


# функция запроса частоты мерцания светодиода
function requestRate {
    clear
    echo "get-led-rate">> $INPUT_FIFO
    read -r -a line < $OUTPUT_FIFO
    if [ ! -z ${line[0]} ] && [ ${line[0]} = "OK" ] && [ ! -z ${line[1]} ]
    then
        echo "*******************************************************" 
        echo -e "\n\tЧастота мерцания светодиода: ${line[1]} Гц\n"
        echo "*******************************************************" 
    else
        echo "*******************************************************" 
        echo -e "\n\tОшибка запроса частоты мерцания светодиода!\n"
        echo "*******************************************************" 
    fi
}


# функция проверки существования каналов ввода-вывода
function checkFifo {
    if [ ! -e $INPUT_FIFO ]
    then
        echo "ОШИБКА: Файл входного канала не существует!"
        echo "Запустите сервер управления светодиодом камеры!"
        exit
    fi

    if [ ! -e $OUTPUT_FIFO ]
    then
        echo "ОШИБКА: Файл выходного канала не существует!"
        echo "Запустите сервер управления светодиодом камеры!"
        exit
    fi
}


# основное тело скрипта

checkFifo

while [ $? -ne 1 ]
    do
        mainMenu
        case $option in
            0)
            break ;;
            1)
            changeSate ;;
            2)
            requestState ;;
            3)
            changeColor ;;
            4)
            requestColor ;;
            5)
            changeRate ;;
            6)
            requestRate ;;
            *)
            clear
            echo "Нужно выбрать функцию!";;
        esac
        echo -en "\n\nДля продолжения нажмите любую клавишу..."
        read -n 1 line
    done
clear
