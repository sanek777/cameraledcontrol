CC = g++

CFLAGS =

LDFLAGS = 

LIBS = 

TARGET = server

OBJS = ledcontroller.o commands.o server.o main.o

.cpp.o:
	$(CC) $(CFLAGS) -Wall -O3 -c -o $@ $<

$(TARGET): $(OBJS) 
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	rm -f $(TARGET) *.o
