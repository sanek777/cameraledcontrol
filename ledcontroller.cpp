#include <vector>
#include "ledcontroller.h"

#define MIN_LED_RATE 0
#define MAX_LED_RATE 5


typedef vector<string> Colors;
static Colors avalableColors;


const string LedController::LED_COLOR_UNDEFINED("UNDEFINED");
const int LedController::LED_RATE_UNDEFINED = -1;

// При старте состояние светодиода неопределено
int LedController::ledState = LED_STATE_UNDEFINED;
int LedController::ledRate = LED_RATE_UNDEFINED;
string LedController::ledColor = LED_COLOR_UNDEFINED;

LedController LedController::ledController;


LedController::LedController()
{
    avalableColors.push_back("red");
    avalableColors.push_back("green");
    avalableColors.push_back("blue");
}


LedController::~LedController()
{
}


bool LedController::setColor(const string &color)
{
    Colors::const_iterator it = avalableColors.begin();
    while(it != avalableColors.end()) {
        if (color == *it) {
            ledColor = color;
            return true;
        }
        ++it;
    }
    return false;
}


string LedController::color()
{
    return ledColor;
}


bool LedController::setState(const int state)
{
    if (state >= LED_STATE_OFF && state < LED_STATE_UNDEFINED) {
        ledState = state;
        return true;
    }
    return false;
}


int LedController::state()
{
    return ledState;
}


bool LedController::setRate(const int rate)
{
    if (rate >= MIN_LED_RATE && rate <= MAX_LED_RATE) {
        ledRate = rate;
        return true;
    }
    return false;
}


int LedController::rate()
{
    return ledRate;
}
