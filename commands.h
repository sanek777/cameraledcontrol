#ifndef COMMAND_H
#define COMMAND_H

#include <string>
#include <iostream>

using namespace std;


/**
 *   Абстрактный класс команды управления светодиодом
 */
class Command
{
protected:
    const string command;   // текстовое представление команды
    const bool hasArgs;     // признак наличия у команды аргуметов
    string commandResult;   // результат выполнения команды
 
    static const string OK;
    static const string FAILED;

protected:
    bool validate(const string &cmd) {
        int cmdLen = cmd.length();
        int thisLen = command.length();

        if (!hasArgs && cmdLen != thisLen)
            return false;

        if (hasArgs && cmdLen <= thisLen)
            return false;

        if (hasArgs && cmd[thisLen] != 0x20)  // у команды с аргументами между именем
            return false;                     // и аргументами должен быть пробел

        return cmd.substr(0, thisLen) == command;
    }

public:
    Command(const char *cmd, const bool hArgs = false): command(cmd), hasArgs(hArgs) {}
    virtual ~Command() {}

    virtual string result() const {
        return commandResult;
    }

    // Функция исполнения команды
    virtual bool execute(const string &cmd) = 0;
};


/**
 *   Класс команды установки состояния светодиода
 */
class SetLedStateCommand: public Command
{
public:
    SetLedStateCommand(): Command("set-led-state", true) {}
    virtual ~SetLedStateCommand() {}

    virtual bool execute(const string &cmd);
};


/**
 *   Класс команды запроса состояния светодиода
 */
class GetLedStateCommand: public Command
{
public:
    GetLedStateCommand(): Command("get-led-state") {}
    virtual ~GetLedStateCommand() {}

    virtual bool execute(const string &cmd);
};


/**
 *   Класс команды установки цвета светодиода
 */
class SetLedColorCommand: public Command
{
public:
    SetLedColorCommand(): Command("set-led-color", true) {}
    virtual ~SetLedColorCommand() {}

    virtual bool execute(const string &cmd);
};


/**
 *   Класс команды запроса цвета светодиода
 */
class GetLedColorCommand: public Command
{
public:
    GetLedColorCommand(): Command("get-led-color") {}
    virtual ~GetLedColorCommand() {}

    virtual bool execute(const string &cmd);
};


/**
 *   Класс команды установки частоты мерцания светодиода
 */
class SetLedRateCommand: public Command
{
public:
    SetLedRateCommand(): Command("set-led-rate", true) {}
    virtual ~SetLedRateCommand() {}

    virtual bool execute(const string &cmd);
};


/**
 *   Класс команды запроса частоты мерцания светодиода
 */
class GetLedRateCommand: public Command
{
public:
    GetLedRateCommand(): Command("get-led-rate") {}
    virtual ~GetLedRateCommand() {}

    virtual bool execute(const string &cmd);
};


#endif // COMMAND_H
